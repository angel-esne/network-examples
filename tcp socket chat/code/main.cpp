
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                             *
 *  Started by Ángel on april of 2020                                          *
 *                                                                             *
 *  This is free software released into the public domain.                     *
 *                                                                             *
 *  angel.rodriguez@esne.edu                                                   *
 *                                                                             *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <SFML/Network.hpp>

using namespace sf;
using namespace std;

namespace
{

    enum UserSelection
    {
        CONNECT,
        WAIT_FOR_CONNECTION
    };

    // ------------------------------------------------------------------------------------------ //

    UserSelection prompt_user_selection ()
    {
        while (true)
        {
            cout << "Type 'c' and then press intro for connecting or 'w' for waiting ('x' exits): ";

            char c = char(cin.get ());

            switch (c)
            {
                case 'c':
                case 'C':
                {
                    return CONNECT;
                }
                case 'w':
                case 'W':
                {
                    return WAIT_FOR_CONNECTION;
                }
                case 'x':
                case 'X':
                {
                    exit (0);
                }
                default:
                {
                    cout << "Wrong selection (" << c << ")." << endl;
                }
            }
        }
    }

    // ------------------------------------------------------------------------------------------ //

    IpAddress prompt_for_ip_address ()
    {
        string  address;

        cout << "Type the target address (IP, domain name or localhost) and press intro: ";
        cin  >> address;

        if (address == "localhost" || address == "LocalHost" || address == "LOCALHOST")
        {
            return IpAddress::LocalHost;
        }

        return IpAddress(address);
    }

    // ------------------------------------------------------------------------------------------ //

    uint16_t prompt_for_port ()
    {
        while (true)
        {
            string  port_string;

            cout << "Type the port number and press intro: ";
            cin  >> port_string;

            try
            {
                int port = stoi (port_string);

                if (port < 0 || port > 65536)
                {
                    cout << "Port number out of range." << endl;
                }
                else
                {
                    return (uint16_t)port;
                }
            }
            catch (...)
            {
                cout << "Wrong port number format." << endl;
            }
        }
    }

    // ------------------------------------------------------------------------------------------ //

    void show_messaging_help ()
    {
        cout << "Type a message and then press intro to send it." << endl;
        cout << "Type X to exit." << endl;
    }

    // ------------------------------------------------------------------------------------------ //

    /** Esta función permite establecer una conexión con otra instancia de la aplicación
      * que se esté ejecutando y esperando conexiones en el mismo ordenador (localhost:port)
      * o en otro (host:port).
      */
    bool connect (TcpSocket & socket)
    {
        IpAddress ip   = prompt_for_ip_address ();
        uint16_t  port = prompt_for_port ();

        cout << "Connecting to " << ip.toString () << ":" << port << "..." << endl;

        if (socket.connect (ip, port) == Socket::Done)
        {
            cout << "Connected." << endl;

            show_messaging_help ();

            return true;
        }

        cout << "Connection failed." << endl;

        return false;
    }

    // ------------------------------------------------------------------------------------------ //

    /** Esta función deja el programa a la espera de que otra instancia inicie una conexión
      * en la direción y puerto de escucha.
      */
    bool wait_for_connection (TcpSocket & socket)
    {
        uint16_t port = prompt_for_port ();

	    TcpListener listener;

        if (listener.listen (port) == Socket::Done)
        {
            cout << "Waiting for connections to localhost (" << IpAddress::getLocalAddress () << ") on port " << port << "." << endl;

	        if (listener.accept (socket) == Socket::Done)
            {
    	        cout << "Received connection from " << socket.getRemoteAddress () << endl;

                show_messaging_help ();

                return true;
            }
            else
            {
                cout << "Failed handling an incoming connection." << endl;
            }
        }
        else
        {
            cout << "Failed to listen for incoming connections." << endl;
        }

        return false;
    }

    // ------------------------------------------------------------------------------------------ //

    /** Esta función se ejecutará en un nuevo hilo para gestionar el envío y recepción de
      * mensajes sin bloquear ni dejarse bloquear por el hilo principal.
      */
    void handle_connection
    (
        TcpSocket * socket,             // Objeto que representa y controla la conexión de red
        bool      * exit,               // Variable compartida con el hilo principal para controlar la terminación
        string    * output_message,     // Cadena que el hilo principal llena y este hilo envía y luego vacía
        mutex     * message_mutex       // Mutex compartido con el hilo principal para evitar el acceso concurrente a output_message
    )
    {
        // Esta función se invoca cuando se ha perdido la conexión con la instancia remota:

        auto disconnected = [&exit] ()
        {
            cout << "The socket has been disconnected." << endl;
            cout << "Press intro to exit." << endl;

           *exit = true;
        };

        // Esta función se invoca cuando se ha producido algún error durante la conexión:

        auto failed = [&exit] ()
        {
            cout << "An unexpected error occurred." << endl;

           *exit = true;
        };

        // Este es el bucle que impide que el hilo se cierre (cosa que ocurre automáticamente
        // al salir de esta función):

        while (!*exit)
        {
            this_thread::sleep_for (100ms);

            // Es necesario bloquear el mutex para evitar leer o modificar message mientras el
            // hilo principal lo esté usando:

            lock_guard< mutex > lock(*message_mutex);

            if (!output_message->empty ())
            {
                // Si output_message no está vacío, se intenta enviar su contenido:

                Packet output_data;

                output_data << *output_message;

                auto status = socket->send (output_data);

                switch (status)
                {
                    case TcpSocket::Done:
                    {
                        // Si el envío se ha realizado con éxito, se borra la cadena (indicando
                        // implícitamente que el mensaje se ha enviado y se puede pedir otro):

                        output_message->clear ();

                        break;
                    }

                    case TcpSocket::Partial:
                    case TcpSocket::NotReady:       // Timeout?
                    {
                        // El paquete se debe enviar de nuevo. Simplemente no borrando la cadena
                        // output_message, se volverá a intentar enviar automáticamente en la
                        // siguiente iteración del bucle de este hilo.

                        break;
                    }

                    case TcpSocket::Disconnected:
                    {
                        disconnected ();
                        break;
                    }

                    case TcpSocket::Error:
                    {
                        failed ();
                        break;
                    }
                }
            }

            // Se desactiva el bloqueo del socket para que la llamada a receive() retorne
            // inmediatamente si no se ha recibido nada:

            socket->setBlocking (false);

            Packet input_data;
            string input_message;

            auto status = socket->receive (input_data);

            switch (status)
            {
                case TcpSocket::Done:
                {
                    input_data >> input_message;

                    if (!input_message.empty ())
                    {
                        cout << "Remote: " << input_message << endl;
                    }

                    break;
                }

                case TcpSocket::Disconnected:
                {
                    disconnected ();
                    break;
                }

                case TcpSocket::Error:
                {
                    failed ();
                    break;
                }
            }

            // Se restaura el bloqueo para que la próxima llamada a send() espere a que se haya
            // enviado el paquete completo:

            socket->setBlocking (true);
        }
    }

}

// ---------------------------------------------------------------------------------------------- //

int main ()
{
    // Se pregunta al usario qué hacer y, según lo que decida, se espera a recibir una
    // conexión o se intenta establecer una conexión:

    TcpSocket socket;

    auto user_selection =  prompt_user_selection ();

    if  (user_selection == CONNECT)
    {
        if (!connect (socket))
        {
            return -1;
        }
    }
    else
    if (user_selection == WAIT_FOR_CONNECTION)
    {
        if (!wait_for_connection (socket))
        {
            return -1;
        }
    }

    // Llegados a este punto se tiene una conexión. Se crea e inicia un hilo para gestionarla
    // y se entra en el bucle que permite enviar mensajes:

    string message;
    bool   exit = false;

    mutex  message_mutex;
    thread socket_thread(handle_connection, &socket, &exit, &message, &message_mutex);

    while (!exit)
    {
        // Cuando message tiene un texto significa que este todavía no se ha llegado a enviar.
        // Tras enviarlo, el hilo del socket lo borrará y se podrá volver a pedir otro.

        if (message.empty ())
        {
            // Se espera a que el usuario introduzca un texto y pulse intro:

            string buffer;

            getline (cin, buffer);

            // Si se teclea el comando X (salir) se termina el programa:
            // También puede ocurrir que exit valga true si se pierde la conexión.

            if (exit || buffer == "x" || buffer == "X")
            {
                cout << "Leaving.";

                exit = true;
            }
            else
            {
                // Es necesario bloquear el mutex para evitar modificar message mientras el
                // hilo del socket lo esté usando. Una vez se logra tomar control del mutex,
                // se actualiza el mensaje:
                {
                    lock_guard< mutex > lock(message_mutex);

                    message = buffer;
                }

                // Después de liberar el mutex, se da un poco de tiempo al otro hilo para
                // que pueda iniciar el envío:

                this_thread::sleep_for (100ms);
            }
        }
    }

    // Se espera a que el hilo del socket termine tras salir de su propio bucle:

    socket_thread.join ();

    return 0;
}
