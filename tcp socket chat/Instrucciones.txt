
Se debe compilar la aplicación y ejecutar dos instancias en el mismo ordenador o
en ordenadores diferentes (que se puedan comunicar a través de una red).

Si el programa se intenta ejecutar en un ordenador en el que no se haya instalado
Visual Studio previamente, podría ser necesario instalar el paquete "Visual C++
redistributable files" que se puede descargar de la página de Microsoft. Hay una
versión del paquete que es compatible con Visual C++ 2015, 2017 y 2019. Se debe
tener cuidado de instalar una versión de 32 bits (x86) o de 64 bits (x64) según
corresponda.
En el momento de escribir este documento se podía descargar aquí:
https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads

En la primera instancia que se inicie se debe elegir la opción 'w' para que espere
a recibir una conexión. Se debe indicar un número de puerto cualquiera (por ejemplo,
el 5000).

En la segunda instancia que se inicie se debe elegir la opción 'c' para conectar con
la primera instancia. Luego se debe indicar la dirección IP del ordenador en el que
se ejecutó la primera instancia (127.0.0.1 o localhost si ambas instancias se ejecutan
en la misma máquina) y el mismo número de puerto.

Con esto se debería conseguir establecer una conexción TCP entre las dos instancias
de la aplicación y se podrían enviar mensajes entre ellas.
