/*
 * Started by Ángel on april of 2022
 * This is free software released into the public domain.
 * angel.rodriguez@esne.edu
 */

global.log = console

const Express    = require ("express")
const Http       = require ("http")
const Controller = require ("./controller")
const port       = 8080

function configureEventListeners (server)
{
    log.info ("Setting up the server.")

    server.on ("error",     (error) => log.error ("Failed to start the server ->", String(error)))
    server.on ("listening", (     ) => log.info  (`Server listening on port ${port}.`))
}

function configureRoutes (express, controller)
{
    log.info ("Configuring the routes.")

    express.get    ("/users",     (request, response) => controller.GET_users       (request, response))
    express.post   ("/users",     (request, response) => controller.POST_users      (request, response))
    express.get    ("/users/:id", (request, response) => controller.GET_users_id    (request, response))
    express.put    ("/users/:id", (request, response) => controller.PUT_users_id    (request, response))
    express.delete ("/users/:id", (request, response) => controller.DELETE_users_id (request, response))
}

function start ()
{
    const express    = Express ()
    const server     = new Http.Server (express)
    const controller = new Controller()

    configureEventListeners (server)
    configureRoutes (express, controller)

    log.info ("Starting the server.")

    server.listen (port)
}

start ()
