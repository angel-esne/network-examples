/*
 * Started by Ángel on april of 2022
 * This is free software released into the public domain.
 * angel.rodriguez@esne.edu
 */

module.exports = class Controller
{

    constructor()
    {
    }

    GET_users (request, response)
    {
        log.info ("Received GET request at /users.")

        response.send ("not implemented")
    }

    async POST_users (request, response)
    {
        log.info ("Received POST request at /users.")

        response.send ("not implemented")
    }

    async GET_users_id (request, response)
    {
        log.info (`Received GET request at /users/${request.params.id}.`)

        response.send ("not implemented")
    }

    async PUT_users_id (request, response)
    {
        log.info (`Received PUT request at /users/${request.params.id}.`)

        response.send ("not implemented")
    }

    async DELETE_users_id (request, response)
    {
        log.info (`Received DELETE request at /users/${request.params.id}.`)

        response.send ("not implemented")
    }

}
