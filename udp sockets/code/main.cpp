/*
 * Started by Ángel on may of 2021
 * This is free software released into the public domain.
 * angel.rodriguez@esne.edu
 */

#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <map>
#include <mutex>
#include <string>
#include <thread>
#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>

using namespace sf;
using namespace std;

namespace
{

    struct Instance
    {
        uint32_t id;
        uint32_t x;
        uint32_t y;
    };

    // ------------------------------------------------------------------------------------------ //

    uint16_t prompt_for_port (const string & message)
    {
        while (true)
        {
            string  port_string;

            cout << message;
            cin  >> port_string;

            try
            {
                int port = stoi (port_string);

                if (port < 0 || port > 65536)
                {
                    cout << "Port number out of range." << endl;
                }
                else
                {
                    return (uint16_t)port;
                }
            }
            catch (...)
            {
                cout << "Wrong port number format." << endl;
            }
        }
    }

    // ------------------------------------------------------------------------------------------ //

    bool bind (UdpSocket & socket)
    {
        uint16_t port = prompt_for_port ("Type the port number and press intro: ");

        cout << "Binding UDP socket to port " << port << "..." << endl;

        if (socket.bind (port) == Socket::Done)
        {
            cout << "Binded." << endl;

            return true;
        }

        cout << "Binding failed." << endl;

        return false;
    }

    // ------------------------------------------------------------------------------------------ //

    void draw (const Instance & instance, RenderWindow & renderer)
    {
        CircleShape shape(25.f);

        shape.setPosition     (Vector2f{ float(instance.x - 25.f), float(instance.y - 25.f) });
        shape.setFillColor    (Color   { instance.id });
        shape.setOutlineColor (Color::Black);
        shape.setOutlineThickness (2.f);

        renderer.draw (shape);
    }

}

// ---------------------------------------------------------------------------------------------- //

int main ()
{
    sf::UdpSocket socket;

    if (bind (socket))
    {
        uint16_t recipient_port = prompt_for_port ("Type the target port number and press intro: ");

        RenderWindow window(VideoMode(800, 600), "UDP sockets example");

        srand (unsigned(time (nullptr)));

        Instance local_instance
        {
            uint32_t( rand ()                 ), 
            uint32_t( window.getSize ().x / 2 ),
            uint32_t( window.getSize ().y / 2 )
        };

        map< uint32_t, Instance > remote_intance_registry;

        bool exit = false;

        do
        {
            Event event;

            while (window.pollEvent (event))
            {
                switch (event.type)
                {
                    case Event::Closed:
                    {
                        exit = true;
                        break;
                    }

                    case Event::MouseMoved:
                    {
                        local_instance.x = uint32_t( event.mouseMove.x );
                        local_instance.y = uint32_t( event.mouseMove.y );
                        break;
                    }
                }
            }

            socket.setBlocking (true);

            socket.send (&local_instance, sizeof(Instance), IpAddress::Broadcast, recipient_port);

            for (int read_count = 0; read_count < 50 && not exit; ++read_count)
            {
                Instance  remote_instance;
                size_t    received;
                IpAddress sender_ip;
                uint16_t  sender_port;

                socket.setBlocking (false);

                if (socket.receive (&remote_instance, sizeof(Instance), received, sender_ip, sender_port) == Socket::Done)
                {
                    if (received == sizeof(Instance) && remote_instance.id != local_instance.id)
                    {
                        remote_intance_registry[remote_instance.id] = remote_instance;
                    }
                }            
            }

            window.clear (Color(128, 128, 128, 255));

            draw (local_instance, window);

            for (auto & item : remote_intance_registry)
            {
                draw (item.second, window);
            }

            window.display ();

            this_thread::sleep_for (10ms);
        }
        while (not exit);
    }

    return 0;
}
